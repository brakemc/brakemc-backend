<?php
abstract class STATUS_CODE
{
    const AUTH_NONE = 0;
	const NO_USERNAME = 1;
	const NO_PASSWORD = 2;
	const WRONG_CREDENTIALS = 3;
	const CREDENTIALS_OK = 100;
}
?>