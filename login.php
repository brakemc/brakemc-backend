<?
header('Content-Type: application/json; charset=utf-8');

include("enum.php");
include("dictionary.php");
include("database.php");

class App
{
    private $ph;

    function __construct()
    {
        $this->ph = new Dictionary();
    }

    public function Run()
    {
        session_start();
        try {
            if (!$_COOKIE && $_POST) {
                $Daten = $_POST;
                if (is_null($Daten["client_auth"]))
                    $this->ph->Add("STATUS", STATUS_CODE::AUTH_NONE);

                if (is_null($Daten["username"]))
                    $this->ph->Add("STATUS", STATUS_CODE::NO_USERNAME);

                if (is_null($Daten["password"]))
                    $this->ph->Add("STATUS", STATUS_CODE::NO_PASSWORD);

                if (is_null($this->ph->Get("STATUS"))) {
                    $login = [
                        "user" => $_POST["username"],
                        "pass" => $_POST["password"]
                    ];

                    if ($login["pass"] = hash("sha512", $login["pass"])) {

                        $db = new mysqli(Database::Host, Database::User, Database::Pass, Database::Data);
                        $statement = $db->prepare("SELECT * FROM Benutzer WHERE Username = ? AND Password = ? LIMIT 1");
                        $statement->bind_param('ss', $login["user"], $login["pass"]);
                        $statement->execute();
                        $result = $statement->get_result();

                        if ($result->num_rows === 0)
                            $this->ph->Add("STATUS", STATUS_CODE::WRONG_CREDENTIALS);
                        else {
                            $this->ph->Add("STATUS", STATUS_CODE::CREDENTIALS_OK);
                            $this->ph->Add("ACTION", ["FORWARD", "query.php"]);
                        }
                    }
                }
                echo json_encode($this->ph->GetAll());
            } else header('HTTP/1.0 403 Forbidden');
        } catch (Exception $exc) {
            die($exc->getMessage());
        }
    }
}

//Run App!
(new App())->Run();
